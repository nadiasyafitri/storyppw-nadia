from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.http import HttpResponseRedirect
import requests
import json

# Create your views here.

def index(request):
	return render(request, 'books.html')

def data_json(request, search= 'quilting'):
	try :
		search=request.GET["search"]
	except:
		search="quilting"
	getBooksJson = requests.get('https://www.googleapis.com/books/v1/volumes?q='+search)
	jsonParsed = getBooksJson.json()
	return JsonResponse(jsonParsed)

def login(request):
	response = {}
	if request.user.is_authenticated:
		request.session['user'] = request.user.username
		request.session['email'] = request.user.email

		request.session.get('counter', 0)
		print(dict(request.session))
		for key, value in request.session.items():
			print('{} => {}'.format(key, value))
	return render(request, 'login.html', response)


def logout(request):
	request.session.flush()
	return HttpResponseRedirect('/story-9')

def add_Fav(request):
	print(dict(request.session))
	request.session['counter'] = request.session['counter'] + 1
	return HttpResponse(request.session['counter'], content_type = "application/json")

def remove_Fav(request):
	request.session['counter'] = request.session['counter'] - 1
	return HttpResponse(request.session['counter'], content_type = "application/json")


