from django.test import TestCase, Client, LiveServerTestCase
from story9.views import data_json, index
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Create your tests here.

class Story9TestCase (TestCase):
    def test_root_url_is_exist(self):
        response = Client().get('/story-9/')
        self.assertEqual(response.status_code,200)

    def test_story9_using_index_func(self):
        found = resolve('/story-9/')
        self.assertEqual(found.func, index)

    def test_name_on_profile_page(self):
        response = Client().get('/story-9/')
        html_response = response.content.decode('utf8')
        self.assertIn('Book List', html_response)

class Story9FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        super(Story9FunctionalTest, self).setUp()

    def test_style_background_color(self):
        #Opening the link we want to tests
        self.browser.get(self.live_server_url)
        box_color = self.browser.find_element_by_tag_name('body')
        self.assertIn('rgba(240, 240, 237, 1)', box_color.value_of_css_property('background-color'))

    
