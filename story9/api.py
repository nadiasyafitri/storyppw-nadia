import requests

BOOKS_API = "https://www.googleapis.com/books/v1/volumes?q=quilting"
def get_books():
	return requests.get(BOOKS_API).json()
