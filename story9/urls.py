from django.urls import path

from .views import *

app_name ='story9'
urlpatterns = [
    path('data/', data_json, name='data'),
    path('', index, name ='home'),
]
