from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from django.apps import apps
from .apps import Story10Config

# for Functional Test
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class Story10UnitTest(TestCase):
    def test_root_url_is_exist(self):
        response = Client().get('/story-10/')
        self.assertEqual(response.status_code,200)

    def test_story8_using_index_func(self):
        found = resolve('/story-10/')
        self.assertEqual(found.func, index)

    def test_apps(self):
        self.assertEqual(Story10Config.name, 'story10')
        self.assertEqual(apps.get_app_config('story10').name, 'story10')

    # def test_name_on_profile_page(self):
    #     response = Client().get('/story-8/')
    #     html_response = response.content.decode('utf8')
    #     self.assertIn('Nadia Syafitri', html_response)
