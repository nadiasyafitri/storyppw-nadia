from django.urls import path

from .views import *

app_name ='story10'
urlpatterns = [
    path('', index, name ='home'),
    path('check_email', check_email, name = 'check_email'),
    path('subscribe', subscribe, name = 'subscribe')
]
