$(document).ready(function() {
	$.ajax({
		url: "data",
		datatype: 'json',
		success: function(data){
			var result ='<tr>';
			for(var i = 0; i < data.items.length; i++) {
				result += "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
				"<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
				"<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
				"<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" +
				"<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" +
				"<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
				"<td class='align-middle' style='text-align:center'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/png/512/660/660463.png'>" + "</td></tr>";
			}
			$('tbody').append(result);
		}
	})
});


var counter = 0;
function favorite(clicked_id){
	var button = document.getElementById(clicked_id);
	if(button.classList.contains("checked")){
		button.classList.remove("checked");
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/png/512/660/660463.png';
		counter--;
		document.getElementById("counter").innerHTML = counter;
	}
	else{
		button.classList.add('checked');
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/png/512/148/148839.png';
		counter++;
		document.getElementById("counter").innerHTML = counter;
	}
}

function renderHTML(data){
		console.log("render");
		var books_list = data.items;
					for (var i = 0; i < books_list.length; i++){
						var title = books_list[i].volumeInfo.title;
						var author = books_list[i].volumeInfo.authors;
						author = (author) ? author :'no author'
						var published = books_list[i].volumeInfo.publishedDate;
						var temp = title.replace(/'/g, "\\'");
						var table =
						'<tr class="table-books">'+
						'<td class = "nomor">' + (i+1) + '</td>' +
						'<td class="title">'+title+'</td>'+
						'<td class= "author">'+author+'</td>'+
						'<td class= "published">' + published+'</td>'+
						'<td class= "favourite">' + '<button id = "star" ><i class = "fa fa-star"</i></button>' + '</td>';
						$('tbody').append(table);
					}
	}

var clicks = 0;
var ab=0
$(document).on('click', '#search', function(){
   	$('tbody').empty();
   	var searchisi= $("input").val();

	  $.ajax({
      url: "data/?search="+searchisi,
      datatype: 'json',
      success: function(result){
          console.log("test");
          var books_object = jQuery.parseJSON(JSON.stringify(result));
          console.log(books_object);
          renderHTML(books_object);
      }
    });
})
