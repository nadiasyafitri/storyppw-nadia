"""storyppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views
from django.urls import path, include
from story9.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('Story6.urls')),
    path('story-8/', include('story8.urls')),
    path('story-9/', include('story9.urls')),
    path('story-10/', include('story10.urls')),
    path('login/', login, name = 'login'),
    path('story-9/logout/', logout, name = 'logout'),
    path('auth/', include('social_django.urls', namespace='social')),

]
