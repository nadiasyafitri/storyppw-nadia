from django.db import models
from django.utils.timezone import now
# Create your models here.

class Status(models.Model):
    status = models.CharField(max_length = 300)
    date = models.DateTimeField(default = now)
