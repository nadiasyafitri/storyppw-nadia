from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from django.apps import apps
from .apps import Story6Config

# for Functional Test
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class Story6UnitTest(TestCase):
    def test_root_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_story6_using_index_func(self):
            found = resolve('/')
            self.assertEqual(found.func, index)

    def test_landing_page_is_completed(self):
        response = Client().get('')
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, apa kabar?', html_response)

    def test_apps(self):
        self.assertEqual(Story6Config.name, 'Story6')
        self.assertEqual(apps.get_app_config('Story6').name, 'Story6')

    def test_story6_post_success_and_render_the_result(self):
        test = 'tralala trilili'
        response_post = Client().post('',{'status':test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_profile_page_is_exist(self):
        response = Client().get('/profile')
        self.assertEqual(response.status_code, 200)

    def test_story6_profile_using_index_func(self):
        found = resolve('/profile')
        self.assertEqual(found.func, profile)

    def test_name_on_profile_page(self):
        response = Client().get('/profile')
        html_response = response.content.decode('utf8')
        self.assertIn('Nadia Syafitri', html_response)

class Story6FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        super(Story6FunctionalTest, self).setUp()

    def test_input(self):
        #Opening the link we want to test
        self.browser.get(self.live_server_url)
        #Find the form elements
        isiform = self.browser.find_element_by_id('id_status')
        submit = self.browser.find_element_by_id('submit')

        #Fill the form
        isiform.send_keys('Coba Coba')

        # submitting the form
        submit.send_keys(Keys.RETURN)

    def test_style_background_color(self):
        #Opening the link we want to tests
        self.browser.get(self.live_server_url)
        box_color = self.browser.find_element_by_tag_name('body')
        self.assertIn('rgba(240, 240, 237, 1)', box_color.value_of_css_property('background-color'))

    def test_css_selector_section(self):
        self.browser.get(self.live_server_url)
        section_color = self.browser.find_element_by_tag_name('section')
        self.assertIn('rgba(0, 0, 0, 0)', section_color.value_of_css_property('background-color'))

    def test_button_position(self):
        self.browser.get(self.live_server_url)
        button = self.browser.find_element_by_tag_name('button')
        self.assertEqual({'x':331, 'y': 437}, button.location)

    def test_section_postion(self):
        self.browser.get(self.live_server_url)
        section = self.browser.find_element_by_tag_name('section')
        self.assertEqual({'x':10, 'y': 183}, section.location)
