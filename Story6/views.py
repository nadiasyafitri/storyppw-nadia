from django.shortcuts import render
from .models import *
from . import forms
from .forms import *
from django.http import HttpResponseRedirect

# Create your views here.

def index(request):
    response = {}
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        status = Status(status = response['status'])
        status.save()
        return HttpResponseRedirect('/')
    status_list = Status.objects.all()
    response = {'form': form, 'status_list': status_list}
    return render(request, 'home2.html' , response)

def profile(request):
    response = {}
    return render(request, 'profile.html', response)
