from django.urls import path

from .views import *

app_name ='Story6'
urlpatterns = [
    path('', index, name ='home'),
    path('profile',profile, name ='profile')
]
