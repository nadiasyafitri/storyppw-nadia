from django import forms
from .models import Status

class Status_Form(forms.Form):
    attrs = {
        'class' : 'form-control'
        }
    status = forms.CharField(label = '', required = True, max_length= 300,
                           empty_value= 'Anonymous',widget= forms.Textarea(attrs=attrs))
