var emailIsValid = false;

$(document).ready(function(){
  $("#id_email").keyup(function(){
    $("#email_notice").html('');
    check_email();
  });
  $('input').focusout(function(){
    checkAll();
  });
});

function check_email(){
  data = {
    "email" : $("#id_email").val(),
    "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
  }

  $.ajax({
    url: 'check_email',
    data : data,
    method: "POST",
    dataType : 'json',
    success: function(data){
      if(data['success'] == false){
        $("#email_notice").append('<small style="color:red"> Format Email Salah </small>')
      }
    }
  });

  $("#id_button").click(function() {
       event.preventDefault();
       $.ajax({
           headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
           url:"subscribe",
           data: $("").serialize(),
           method: 'POST',
           dataType: 'json',
           success: function(data){
               alert("Hi! Thanks for subscribing. :)");
               document.getElementById('id_name').value = '';
               document.getElementById('id_email').value = '';
               document.getElementById('id_password').value = '';
               $("#id_button").prop("disabled", true);
           }
       })
   });


  // $().on('click', '#id_button', function(){
  //    	var searchisi= $("input").val();
  //
  // 	  $.ajax({
  //       url: "subscribe",
  //       datatype: 'json',
  //       success: function(result){
  //
  //       }
  //     });
  // })

}

function checkAll(){
  if($('#id_name').val()!== "" && $('#id_email').val() !== "" && $('#id_password').val() !== ""){
    $('#id_button').prop("disabled", false);
  }else{
    $('#id_button').prop("disabled", true);
  }
}
