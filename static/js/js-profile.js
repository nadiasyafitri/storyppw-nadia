
$(document).ready(function(){
  $(".se-pre-con").delay(2000).fadeOut("slow");
  $("button").click(function(){
    var n = $('body').css('background-color');

    $('body').css('background-color', '#fbc3ca');

    $('button').click(function(){
        $('body').css('background-color', n);
    })
  })
})

$(function() {
    function toggleChevron(e) {
        $(e.target)
                .prev('.panel-heading')
                .find("i")
                .toggleClass('rotate-icon');
        $('.panel-body.animated').toggleClass('zoomIn zoomOut');
    }

    $('#accordion').on('hide.bs.collapse', toggleChevron);
    $('#accordion').on('show.bs.collapse', toggleChevron);
})

var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 3000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}
